package domein;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class RoosterFactory
{
	private  char[][] rooster;
	
	private final  char PACMAN = 'P';
	private final  char BROODKRUIMEL = '.';
	private final  char MUUR = '%';
	
	private final  Object[] KRUIMELMINI   = {"resources/roosters/kruimelMini.txt",    7,  7};
	private final  Object[] KRUIMELKLEIN  = {"resources/roosters/kruimelKlein.txt",  22, 10};
	private final  Object[] KRUIMELMEDIUM = {"resources/roosters/kruimelMedium.txt", 36, 18};
	private final  Object[] KRUIMELGROOT  = {"resources/roosters/kruimelGroot.txt",  37, 37};
	private final  Object[] VEELMINI   	  = {"resources/roosters/veelMini.txt",    	9,  7};
	private final  Object[] VEELKLEIN  	  = {"resources/roosters/veelKlein.txt",     20,  5};
	private final  Object[] VEELMEDIUM 	  = {"resources/roosters/veelMedium.txt",	   31,  8};
	private final  Object[] VEELGROOT  	  = {"resources/roosters/veelGroot.txt",	   31, 15};
	
	private  Locatie pacmanPos;
	private  List<Locatie> kruimelsPos = new ArrayList<>();

	public  char[][] maakRandomRooster(int size)
	{
		rooster = new char[size][size];
		
		zetPacman(1, 1);
		
		zetBroodKruimel(size-2, size/2);
		
		zetOmringendeMuren(size);
		
		zetRandomBinnenMuren(size, 0.25);
		
		return rooster;
	}
	
	public  char[][] maakRoosterVanBestand(String bestandsnaam)
	{
		switch(bestandsnaam.toLowerCase())
		{
			case "kruimelmini":
				rooster = new char[(int) KRUIMELMINI[2]][(int) KRUIMELMINI[1]];
				leesRoosterVanBestand(KRUIMELMINI); break;
			
			case "kruimelklein":
				rooster = new char[(int) KRUIMELKLEIN[2]][(int) KRUIMELKLEIN[1]];
				leesRoosterVanBestand(KRUIMELKLEIN); break;
				
			case "kruimelmedium":
				rooster = new char[(int) KRUIMELMEDIUM[2]][(int) KRUIMELMEDIUM[1]];
				leesRoosterVanBestand(KRUIMELMEDIUM); break;
				
			case "kruimelgroot":
				rooster = new char[(int) KRUIMELGROOT[2]][(int) KRUIMELGROOT[1]];
				leesRoosterVanBestand(KRUIMELGROOT); break;
				
			case "veelmini":
				rooster = new char[(int) VEELMINI[2]][(int) VEELMINI[1]];
				leesRoosterVanBestand(VEELMINI); break;
			
			case "veelklein":
				rooster = new char[(int) VEELKLEIN[2]][(int) VEELKLEIN[1]];
				leesRoosterVanBestand(VEELKLEIN); break;
				
			case "veelmedium":
				rooster = new char[(int) VEELMEDIUM[2]][(int) VEELMEDIUM[1]];
				leesRoosterVanBestand(VEELMEDIUM); break;
				
			case "veelgroot":
				rooster = new char[(int) VEELGROOT[2]][(int) VEELGROOT[1]];
				leesRoosterVanBestand(VEELGROOT); break;
				
			default: throw new IllegalArgumentException("Bestand bestaat niet");
		}
		
		return rooster;
	}
	
	public  void printRooster(char[][] rooster)
	{
		for(int rij = 0; rij<rooster.length; rij ++)
		{
			for(int kolom = 0; kolom<rooster[rij].length; kolom++)
			{
				System.out.print((rooster[rij][kolom]=='\u0000'?" ":rooster[rij][kolom]));
			}
			System.out.println();
		}
	}
	
	public  Locatie getPacmanPos() {
		return pacmanPos;
	}
	
	public  List<Locatie> getKruimelsPos(){
		return kruimelsPos;
	}
	
	private  void zetPacman(int rij, int kolom)
	{
		rooster[rij][kolom] = PACMAN;
		pacmanPos = new Locatie(rij, kolom);
	}
	
	private  void zetBroodKruimel(int rij, int kolom)
	{
		rooster[rij][kolom] = BROODKRUIMEL;
		kruimelsPos.add(new Locatie(rij,kolom));
	}
	
	private  void zetOmringendeMuren(int size)
	{
		//Bovenste rij
		for(int kolom = 0; kolom < rooster.length; kolom++)
			rooster[0][kolom] = MUUR;
		
		//Onderste rij
		for(int kolom = 0; kolom < rooster.length; kolom++)
			rooster[size-1][kolom] = MUUR;
		
		//Linkse kolom
		for(int rij = 1; rij < rooster.length-1; rij++)
			rooster[rij][0] = MUUR;
		
		//Rechste kolom
		for(int rij = 1; rij < rooster.length-1; rij++)
			rooster[rij][size-1] = MUUR;
					
	}
	
	private  void zetRandomBinnenMuren(int size, double percent)
	{
		Random rand = new Random();
		int aantalTeConverterenVakken = (int) Math.round(berekenLegeVakjes(size) * percent);
		//Teller
		int aantalGeconverteerdeVakken = 0;
		
		while(aantalTeConverterenVakken > aantalGeconverteerdeVakken){
			int randRij = rand.nextInt(size-2)+1;
			int randKolom = rand.nextInt(size-2)+1;
			if(rooster[randRij][randKolom] != PACMAN && rooster[randRij][randKolom] != MUUR && rooster[randRij][randKolom] != BROODKRUIMEL ) {
				rooster[randRij][randKolom] = MUUR;
				aantalGeconverteerdeVakken++;
			}
		}
	}

	private  int berekenLegeVakjes(int size)
	{
		// oppervlakte - 2 * zijden - 2* (zijde-2) door overlapping - 1 van P  
		return (size * size - 2 * size - 2*(size-2) - 1);
	}
	
	private  void leesRoosterVanBestand(Object[] bestand)
	{
		Scanner scanner = null;
		String locatie  = (String) bestand[0];
		int rijLengte = (int) bestand[2];
		
		try
		{
			scanner = new Scanner(new FileInputStream(locatie));
			
			String line;
			
			for(int rij=0; rij < rijLengte; rij++)
			{
				int kolom = 0;
				if(scanner.hasNextLine())
				{
					line = scanner.nextLine();
					for(char c: line.toCharArray())
					{
						if(c == 'P')
							pacmanPos = new Locatie(rij, kolom);
						if(c == '.')
							kruimelsPos.add(new Locatie(rij, kolom));
						
						rooster[rij][kolom] = c;
						kolom++;
					}
				}
			}
		}
		catch(IOException ioe)
		{
			ioe.printStackTrace();
		}
		finally
		{
			scanner.close();
		}
	}
}
