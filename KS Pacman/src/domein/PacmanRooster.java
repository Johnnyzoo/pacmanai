package domein;

import java.util.ArrayList;
import java.util.List;

import aima.core.agent.Action;
import aima.core.agent.impl.DynamicAction;

public class PacmanRooster {

	public static Action LEFT = new DynamicAction("Left");
	public static Action RIGHT = new DynamicAction("Right");
	public static Action UP = new DynamicAction("Up");
	public static Action DOWN = new DynamicAction("Down");

	private Locatie pacmanPos;
	private List<Locatie> kruimelsPos;

	// Veld
	private char[][] state;

	/*
	 * public PacmanRooster() { this.state = new char[][] {}; }
	 */

	public PacmanRooster(char[][] state, Locatie pacmanPos,
			List<Locatie> kruimelsPos) {
		this.state = new char[state.length][state[0].length];
		for (int i = 0; i < state.length; i++)
			System.arraycopy(state[i], 0, this.state[i], 0, state[i].length);
		this.pacmanPos = pacmanPos;
		this.kruimelsPos = new ArrayList<>(kruimelsPos);
	}

	public PacmanRooster(PacmanRooster copyRooster) {
		this(copyRooster.getState(), copyRooster.getPacmanPos(), copyRooster
				.getKruimelsPos());
	}

	public char[][] getState() {
		return state;
	}

	public Locatie getPacmanPos() {
		return pacmanPos;
	}

	public List<Locatie> getKruimelsPos() {
		return kruimelsPos;
	}

	public boolean canMovePacman(Action where) {
		boolean retVal = true;
		// y = rij, x = kolom
		if (where.equals(LEFT))
			retVal = (state[pacmanPos.getRij()][pacmanPos.getKolom() - 1] != '%');
		if (where.equals(RIGHT))
			retVal = (state[pacmanPos.getRij()][pacmanPos.getKolom() + 1] != '%');
		if (where.equals(UP))
			retVal = (state[pacmanPos.getRij() - 1][pacmanPos.getKolom()] != '%');
		if (where.equals(DOWN))
			retVal = (state[pacmanPos.getRij() + 1][pacmanPos.getKolom()] != '%');

		return retVal;
	}

	public void movePacmanUp() {
		if (state[pacmanPos.getRij() - 1][pacmanPos.getKolom()] != '%')
			setPacmanPos(new Locatie(pacmanPos.getRij() - 1,
					pacmanPos.getKolom()));
	}

	public void movePacmanDown() {
		if (state[pacmanPos.getRij() + 1][pacmanPos.getKolom()] != '%')
			setPacmanPos(new Locatie(pacmanPos.getRij() + 1,
					pacmanPos.getKolom()));
	}

	public void movePacmanLeft() {
		if (state[pacmanPos.getRij()][pacmanPos.getKolom() - 1] != '%')
			setPacmanPos(new Locatie(pacmanPos.getRij(),
					pacmanPos.getKolom() - 1));
	}

	public void movePacmanRight() {
		if (state[pacmanPos.getRij()][pacmanPos.getKolom() + 1] != '%')
			setPacmanPos(new Locatie(pacmanPos.getRij(),
					pacmanPos.getKolom() + 1));
	}

	private void setPacmanPos(Locatie newValue) {
		state[pacmanPos.getRij()][pacmanPos.getKolom()] = ' ';
		if (state[newValue.getRij()][newValue.getKolom()] == '.') {
			for (Locatie l : kruimelsPos) {
				if (l.getRij() == newValue.getRij()
						&& l.getKolom() == newValue.getKolom()) {
					kruimelsPos.remove(l);
					break;
				}
			}
		}
		pacmanPos = newValue;
		state[pacmanPos.getRij()][pacmanPos.getKolom()] = 'P';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if ((o == null) || (this.getClass() != o.getClass())) {
			return false;
		}
		PacmanRooster aRooster = (PacmanRooster) o;
			if (this.getPacmanPos().getRij() != aRooster.getPacmanPos().getRij() || this.getPacmanPos().getKolom() != aRooster.getPacmanPos().getKolom()) {
				return false;
			}
		return true;
	}
	// Moet ook aangepast worden denk ik..
	@Override
	public int hashCode(){
		int result = 17;
		result = result * 13 + kruimelsPos.hashCode();
		//result = result * 17 + pacmanPos.hashCode();
		//result = result * 31 + state.hashCode();
		return result;	
	}
}
