package domein;

import java.util.LinkedHashSet;
import java.util.Set;

import aima.core.agent.Action;
import aima.core.search.framework.ActionsFunction;
import aima.core.search.framework.ResultFunction;

public class PacmanRoosterFunctionFactory {
	
	private static ActionsFunction _actionsFunction = null;
	private static ResultFunction _resultFunction = null;
	
	public static ActionsFunction getActionsFunction() {
		if (null == _actionsFunction) {
			_actionsFunction = new PGActionsFunction();
		}
		return _actionsFunction;
	}

	public static ResultFunction getResultFunction() {
		if (null == _resultFunction) {
			_resultFunction = new PGResultFunction();
		}
		return _resultFunction;
	}
	
	private static class PGActionsFunction implements ActionsFunction {
		public Set<Action> actions(Object state) {
			PacmanRooster rooster = (PacmanRooster) state;

			Set<Action> actions = new LinkedHashSet<Action>();

			if (rooster.canMovePacman(PacmanRooster.DOWN)) {
				actions.add(PacmanRooster.DOWN);
			}
			if (rooster.canMovePacman(PacmanRooster.LEFT)) {
				actions.add(PacmanRooster.LEFT);
			}
			if (rooster.canMovePacman(PacmanRooster.UP)) {
				actions.add(PacmanRooster.UP);
			}
			if (rooster.canMovePacman(PacmanRooster.RIGHT)) {
				actions.add(PacmanRooster.RIGHT);
			}

			return actions;
		}
	}

	private static class PGResultFunction implements ResultFunction {
		public Object result(Object s, Action a) {
			PacmanRooster rooster = (PacmanRooster) s;

			if (PacmanRooster.UP.equals(a)
					&& rooster.canMovePacman(PacmanRooster.UP)) {
				PacmanRooster newrooster = new PacmanRooster(rooster);		
				newrooster.movePacmanUp();
				return newrooster;
			} else if (PacmanRooster.DOWN.equals(a)
					&& rooster.canMovePacman(PacmanRooster.DOWN)) {
				PacmanRooster newrooster = new PacmanRooster(rooster);
				newrooster.movePacmanDown();
				return newrooster;
			} else if (PacmanRooster.LEFT.equals(a)
					&& rooster.canMovePacman(PacmanRooster.LEFT)) {				
				PacmanRooster newrooster = new PacmanRooster(rooster);
				newrooster.movePacmanLeft();
				return newrooster;
			} else if (PacmanRooster.RIGHT.equals(a)
					&& rooster.canMovePacman(PacmanRooster.RIGHT)) {
				PacmanRooster newrooster = new PacmanRooster(rooster);
				newrooster.movePacmanRight();
				return newrooster;
			}
			// The Action is not understood or is a NoOp
			// the result will be the current state.
			return s;
		}
	}
}