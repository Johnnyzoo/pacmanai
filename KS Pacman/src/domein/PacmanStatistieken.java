package domein;

import java.util.Properties;

public class PacmanStatistieken
{
	
	private double gemMaxQSize;
	private double gemNodesExpanded;
	private double gemPathCost;
	
	public PacmanStatistieken(int algoritme, int dimensieGrootte, int aantalKeer)
	{
		berekenStatistieken(algoritme, dimensieGrootte, aantalKeer);
		printStatistieken();
	}
	
	
	
	private void berekenStatistieken(int algoritme, int dimensieGrootte, int aantalKeer)
	{
		PacmanDemo pacmanDemo = new PacmanDemo();
		Properties agentProperties;
		
		double totMaxQSize = 0.0;
		double totNodesExpanded = 0.0;
		double totPathCost = 0.0;
		
		for(int i=1; i<=aantalKeer; i++)
		{
			System.out.println(i + "e keer: ");
			agentProperties = pacmanDemo.getProperties((int) i, dimensieGrootte);
			
			totMaxQSize 	 += Double.valueOf(agentProperties.getProperty("maxQueueSize"));
			totNodesExpanded += Double.valueOf(agentProperties.getProperty("nodesExpanded"));
			totPathCost		 += Double.valueOf(agentProperties.getProperty("pathCost"));
		}
		
		gemMaxQSize = totMaxQSize / aantalKeer;
		gemNodesExpanded = totNodesExpanded / aantalKeer;
		gemPathCost	= totPathCost		/ aantalKeer;
	}
	
	private void printStatistieken()
	{
		System.out.println();
		System.out.println("Gemiddeldes:");
		System.out.println("Maximum Queue Size: " + gemMaxQSize);
		System.out.println("Nodes Expanded: " + gemNodesExpanded);
		System.out.println("Path Cost: " + gemPathCost);
	}
}
