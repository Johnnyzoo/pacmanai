package domein;

public class Locatie {
	
	private int rij;
	private int kolom;
	
	public Locatie(int rij, int kolom) {
		this.rij = rij;
		this.kolom = kolom;
	}
	
	public int getRij() {
		return rij;
	}

	public void setRij(int rij) {
		this.rij = rij;
	}

	public int getKolom() {
		return kolom;
	}

	public void setKolom(int kolom) {
		this.kolom = kolom;
	}


	


}
