package domein;

import aima.core.agent.Action;
import aima.core.search.framework.StepCostFunction;

public class StepCostFunctionW implements StepCostFunction{
	
	public StepCostFunctionW()
	{
		
	}
	
	// Betreden van positie (x,y) kost:
	// w(x,y) = 2^x <-- X = kolom
	@Override
	public double c(Object fromCurrentState, Action action, Object toNextState) {
		PacmanRooster from = (PacmanRooster) fromCurrentState;
		PacmanRooster to = (PacmanRooster) toNextState;		
		double cost = Math.pow(2.0, to.getPacmanPos().getKolom());
		System.out.println(String.format("Pacman Kolom: %d, Kost voor de stap: %f", to.getPacmanPos().getKolom(), cost));
		return cost;
	}
}
