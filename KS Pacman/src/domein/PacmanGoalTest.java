package domein;

import aima.core.search.framework.GoalTest;

public class PacmanGoalTest implements GoalTest
{
	/*
	@Override
	public boolean isGoalState(Object state)
	{
		return (((PacmanRooster) state).getKruimelsPos().isEmpty());
	}
	*/
	
	
	
	@Override
	public boolean isGoalState(Object state)
	{
		PacmanRooster pr = (PacmanRooster) state;
		
		if(pr.getKruimelsPos().isEmpty())
		{
			new RoosterFactory().printRooster(pr.getState());
			System.out.println();
			return true;
		}
		
		return false;
	}
	
	
}
