package domein;

import aima.core.agent.Action;
import aima.core.search.framework.StepCostFunction;

public class StepCostFunctionE implements StepCostFunction{

	public StepCostFunctionE()
	{
		
	}
	
	// Betreden van positie (x,y) kost:
	// e(x,y) = (1/2)^x  <-- X = kolom
	@Override
	public double c(Object fromCurrentState, Action action, Object toNextState) {
		PacmanRooster from = (PacmanRooster) fromCurrentState;
		PacmanRooster to = (PacmanRooster) toNextState;	
		double cost = Math.pow(0.5, to.getPacmanPos().getKolom());
		System.out.println(String.format("Pacman Kolom: %d, Kost voor de stap: %f", to.getPacmanPos().getKolom(), cost));
		return cost;
	}

}
