package domein;

import aima.core.search.framework.HeuristicFunction;

public class BreadcrumbsLeftHeuristicFunction implements HeuristicFunction
{

	@Override
	public double h(Object state)
	{
		return getNumberOfBreadcrumbsLeft((PacmanRooster) state);
	}
	
	private int getNumberOfBreadcrumbsLeft(PacmanRooster rooster)
	{
		return rooster.getKruimelsPos().size();
	}

}
