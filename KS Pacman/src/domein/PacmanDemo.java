package domein;

import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import aima.core.agent.Action;
import aima.core.search.framework.GraphSearch;
import aima.core.search.framework.Problem;
import aima.core.search.framework.Search;
import aima.core.search.framework.SearchAgent;
import aima.core.search.framework.TreeSearch;
import aima.core.search.informed.AStarSearch;
import aima.core.search.uninformed.BreadthFirstSearch;
import aima.core.search.uninformed.DepthFirstSearch;
import aima.core.search.uninformed.IterativeDeepeningSearch;

public class PacmanDemo
{
	private Search search;
	private char[][] rooster;
	private RoosterFactory factory;
	
	public PacmanDemo()
	{
		factory = new RoosterFactory();
	}
	
	public void StartDemo(int algoritme, int rooster)
	{
		switch(algoritme)
		{
			case 1: this.search = new BreadthFirstSearch(new TreeSearch());		break;
			case 2: this.search = new BreadthFirstSearch(new GraphSearch());	break;
			case 3: this.search = new DepthFirstSearch(new GraphSearch());		break;
			case 4: this.search = new IterativeDeepeningSearch();				break;
			case 5: this.search = new AStarSearch(new GraphSearch(),
									   new BreadcrumbsLeftHeuristicFunction()); break;
		}
		
		switch(rooster)
		{
			case 1: this.rooster = factory.maakRoosterVanBestand("kruimelmini");		break;
			case 2: this.rooster = factory.maakRoosterVanBestand("kruimelklein");	break;
			case 3: this.rooster = factory.maakRoosterVanBestand("kruimelmedium");	break;
			case 4: this.rooster = factory.maakRoosterVanBestand("kruimelgroot");	break;
			case 5: this.rooster = factory.maakRoosterVanBestand("veelmini");		break;
			case 6: this.rooster = factory.maakRoosterVanBestand("veelklein");		break;
			case 7: this.rooster = factory.maakRoosterVanBestand("veelmedium");		break;
			case 8: this.rooster = factory.maakRoosterVanBestand("veelgroot");		break;
		}
		
		startAlgoritme();
	}
	
	public Properties getProperties(int algoritme, int dimensie)
	{
		switch(algoritme)
		{
			case 1: this.search = new BreadthFirstSearch(new TreeSearch());		break;
			case 2: this.search = new BreadthFirstSearch(new GraphSearch());	break;
			case 3: this.search = new DepthFirstSearch(new GraphSearch());		break;
			case 4: this.search = new IterativeDeepeningSearch();				break;
			case 5: this.search = new AStarSearch(new GraphSearch(),
									   new BreadcrumbsLeftHeuristicFunction()); break;
		}
		
		this.rooster = factory.maakRandomRooster(dimensie);
		
		return getAgentVoorRandomRooster().getInstrumentation();
	}
	
	private void startAlgoritme()
	{
		PacmanRooster pacmanRooster = new PacmanRooster(rooster, factory.getPacmanPos(), factory.getKruimelsPos());
		
		factory.printRooster(pacmanRooster.getState());
		System.out.println();
		
		try
		{
			Problem problem = new Problem(pacmanRooster,
										  PacmanRoosterFunctionFactory.getActionsFunction(),
										  PacmanRoosterFunctionFactory.getResultFunction(),
										  new PacmanGoalTest()
										//, new StepCostFunctionW()
										);
			
			SearchAgent agent = new SearchAgent(problem, search);
			
			printActions(agent.getActions());
			printInstrumentation(agent.getInstrumentation());
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	private SearchAgent getAgentVoorRandomRooster()
	{
		SearchAgent agent = null;
		PacmanRooster pacmanRooster = new PacmanRooster(rooster, factory.getPacmanPos(), factory.getKruimelsPos());
		
		factory.printRooster(pacmanRooster.getState());
		//System.out.println();
		
		try
		{
			Problem problem = new Problem(pacmanRooster,
										  PacmanRoosterFunctionFactory.getActionsFunction(),
										  PacmanRoosterFunctionFactory.getResultFunction(),
										  new PacmanGoalTest());
			
			agent = new SearchAgent(problem, search);
			
			printActions(agent.getActions());
			//printInstrumentation(agent.getInstrumentation());
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return agent;
	}
	
	private void printInstrumentation(Properties properties)
	{
		Iterator<Object> keys = properties.keySet().iterator();
		while (keys.hasNext())
		{
			String key = (String) keys.next();
			String property = properties.getProperty(key);
			System.out.println(key + " : " + property);
		}
	}

	private void printActions(List<Action> actions)
	{
		String oplossing = "";
		System.out.println("Oplossing gevonden:");
		for (int i = 0; i < actions.size(); i++)
		{
			String action = actions.get(i).toString().substring(13, actions.get(i).toString().length()-1).toUpperCase();
			oplossing = (oplossing + action + " " + ((i+1)%10==0?"\n":""));
		}
		System.out.println(oplossing + "\n");
	}
}
