import java.util.Scanner;

import domein.PacmanDemo;
import domein.PacmanStatistieken;

public class StartUp
{
	private static Scanner scanner = new Scanner(System.in);
	
	public static void main(String[] args)
	{	
		while(true)
		{
			System.out.println("\nKies een actie.");
			System.out.println("1: PacmanDemo starten");
			System.out.println("2: Statistieken berekenen (door onbekende bug werkt dit alleen maar voor 3 keer)");
			System.out.println("0: Stoppen");
			
			switch(scanner.nextInt())
			{
				case 1: demoKeuze();					 break;
				case 2: statistiekenKeuze();			 break;
				case 0: scanner.close(); System.exit(1); break;
				default: System.out.println("Niet herkende actie");
			}
		}
	}
	
	private static void demoKeuze()
	{
		int algoritme;
		int rooster;
		do
		{
			System.out.println("Welk algoritme wilt u starten?");
			System.out.println("1: BFS Tree");
			System.out.println("2: BFS Graph");
			System.out.println("3: DFS");
			System.out.println("4: Iterative Deepening");
			System.out.println("5: A* Search");
			algoritme = scanner.nextInt();
			
			System.out.println("Welk rooster wilt u gebruiken?");
			System.out.println("1: kruimelMini");
			System.out.println("2: kruimelKlein");
			System.out.println("3: kruimelMedium");
			System.out.println("4: kruimelGroot");
			System.out.println("5: veelMini");
			System.out.println("6: veelKlein");
			System.out.println("7: veelMedium");
			System.out.println("8: veelGroot");
			rooster = scanner.nextInt();
		}
		while(!(algoritme >= 1) || !(algoritme <= 5) || !(rooster >= 1) || !(rooster <= 8));
		
		new PacmanDemo().StartDemo(algoritme, rooster);
	}
	
	private static void statistiekenKeuze()
	{
		int algoritme;
		int dimensieGrootte;
		int aantalKeerPerDimensie;
		do
		{
			System.out.println("Van welk algoritme wilt u statistieken berekenen?");
			System.out.println("1: BFS Tree");
			System.out.println("2: BFS Graph");
			System.out.println("3: DFS");
			System.out.println("4: Iterative Deepening");
			System.out.println("5: A* Search");
			algoritme = scanner.nextInt();
			
			System.out.println("Geef de grootte van de dimensie van de roosters ( > 0 )");
			dimensieGrootte = scanner.nextInt();
			
			System.out.println("Geef het aantal keer dat u het algoritme wilt uitvoeren ( > 0 )");
			aantalKeerPerDimensie = scanner.nextInt();
		}
		while(!(algoritme>=1) && !(algoritme<=5) && !(dimensieGrootte > 0) && !(aantalKeerPerDimensie > 0));
		
		new PacmanStatistieken(algoritme, dimensieGrootte, aantalKeerPerDimensie);
	}
}
